﻿Imports System.Web
Imports System.Web.Services
Imports System.Web.Services.Protocols
Imports AdvWare.Redback
Imports System.Data
Imports System.IO
Imports System.Diagnostics

' To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line.
' <System.Web.Script.Services.ScriptService()> _
<WebService(Namespace:="AWGetAuthorization.ascx/")> _
<WebServiceBinding(ConformsTo:=WsiProfiles.BasicProfile1_1)> _
<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Public Class GetHMEAuthorizationService
    Inherits System.Web.Services.WebService

    <WebMethod()> _
    Public Function GetAthorization(ByVal PartNumber As String, ByVal SerialNumber As String, _
                                    ByVal InstallID As String, ByVal CustomerID As String, _
                                    ByVal ContactName As String, ByVal ContactPhone As String, _
                                    ByVal CompanyCode As String) As String
        Dim Emsg As String = ""
        Dim Alg As String = ValidatePartSerial(PartNumber, SerialNumber, Emsg)
       
        If Emsg = "" And Alg <> "" Then

            Dim AuthCode As String = GetAuthCode(InstallID, SerialNumber, Alg, Emsg)

            If AuthCode <> "" And Emsg = "" Then
                Dim objRedback As New RBRpcClient(System.Configuration.ConfigurationManager.AppSettings("dataprovider"), System.Configuration.ConfigurationManager.AppSettings("dataaccount"))

                Dim userMsg, errorMsg As String

                With objRedback
                    .Command = "W$Auth_UpdateAuth"

                    .Parameter(1) = 1
                    .Parameter(2) = "Get"
                    .Parameter(3) = PartNumber
                    .Parameter(4) = SerialNumber
                    .Parameter(5) = InstallID
                    .Parameter(6) = CustomerID
                    .Parameter(7) = ContactName
                    .Parameter(8) = ContactPhone
                    .Parameter(9) = AuthCode
                    .Parameter(10) = ""
                    .Parameter(11) = CompanyCode

                    .ClearFields()
 
                    userMsg = .UserMessage
                    errorMsg = .ErrorMessage
                End With
                ' This Web Service returns a | "pipe" delimited string with position 1 containing any Error Message and
                '  position 2 (only if Error Message is null) containing the Avante Labor Transaction ID
                If errorMsg.Length = 0 Then
                    GetAthorization = AuthCode
                Else
                    GetAthorization = errorMsg
                End If

                objRedback = Nothing
            Else
                Return Emsg
            End If
        Else
            Return Emsg
        End If




    End Function
    Private Function GetAuthCode(ByVal InstallID As String, ByVal SerialNo As String, ByVal Alg As String, ErrorMessage As String) As String
        GetAuthCode = ""
        HttpContext.Current.Application.Lock()
        Try

            If File.Exists(ConfigurationManager.AppSettings("AuthWorkingDirectory") & "\AuthCODE.txt") Then
                File.Delete(ConfigurationManager.AppSettings("AuthWorkingDirectory") & "\AuthCODE.txt")
            End If

            Dim psinfo As New ProcessStartInfo
            Dim Proc As New Process

            psinfo.UseShellExecute = False
            psinfo.WorkingDirectory = ConfigurationManager.AppSettings("AuthWorkingDirectory")
            psinfo.FileName = ConfigurationManager.AppSettings("AuthExe")
            psinfo.Arguments = InstallID & " " & SerialNo & " " & Alg

            psinfo.RedirectStandardError = True
            psinfo.RedirectStandardOutput = True
            psinfo.RedirectStandardInput = True
            Proc.StartInfo = psinfo
            Proc.Start()
            System.Threading.Thread.Sleep("3000")
            Proc.Kill()
            Dim thefile As StreamReader = File.OpenText(ConfigurationManager.AppSettings("AuthWorkingDirectory") & "\AuthCODE.txt")
            GetAuthCode = thefile.ReadToEnd
            thefile.Close()
            If File.Exists(ConfigurationManager.AppSettings("AuthWorkingDirectory") & "\AuthCODE.txt") Then
                File.Delete(ConfigurationManager.AppSettings("AuthWorkingDirectory") & "\AuthCODE.txt")
            End If
            If GetAuthCode.IndexOf(" ") > 0 Then
                ErrorMessage = "Problem obtaining authorization code. Please contact HME to obtain code. <br />For installations in the US, please call 800.848-4468. All others, please call 858.535.6000. <br />Press 1, 2, 3 to be directed to Technical Support."
                GetAuthCode = ""
            End If
            If GetAuthCode.Length <> 8 Then
                ErrorMessage = "Problem obtaining authorization code. Please contact HME to obtain code. <br />For installations in the US, please call 800.848-4468. All others, please call 858.535.6000. <br />Press 1, 2, 3 to be directed to Technical Support."
                GetAuthCode = ""
            End If
        Catch ex As Exception
            'p_errorMessage = ex.Message
            ErrorMessage = "Problem obtaining authorization code. Please contact HME to obtain code. <br />For installations in the US, please call 800.848-4468. All others, please call 858.535.6000. <br />Press 1, 2, 3 to be directed to Technical Support."
        Finally

        End Try
        HttpContext.Current.Application.UnLock()

    End Function
    Private Function ValidatePartSerial(ByVal PartNumber As String, ByVal SerialNo As String, ByRef ErrorMessage As String) As String
        Dim objRedBack As New RBRpcClient(ConfigurationManager.AppSettings("dataprovider"), ConfigurationManager.AppSettings("dataaccount"))
        With objRedBack
            .Command = "W$Auth_ValidatePartSerial"

            .Parameter(1) = ""
            .Parameter(2) = "Get"
            .Parameter(3) = PartNumber
            .Parameter(4) = SerialNo

            .GetDataTable("default")

            ValidatePartSerial = .UserMessage
            ErrorMessage = .ErrorMessage


        End With
        objRedBack = Nothing
    End Function

End Class
