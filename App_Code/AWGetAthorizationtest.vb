﻿Imports System.Web
Imports System.Web.Services
Imports System.Web.Services.Protocols
Imports AdvWare.Redback
Imports System.Data
Imports System.IO
Imports System.Diagnostics
Imports System.Data.SqlClient

' To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line.
' <System.Web.Script.Services.ScriptService()> _
<WebService(Namespace:="AWGetAuthorization.ascx/")> _
<WebServiceBinding(ConformsTo:=WsiProfiles.BasicProfile1_1)>
<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Public Class GetHMEAuthorizationServicetest
    Inherits System.Web.Services.WebService

    <WebMethod()>
    Public Function GetAthorization(ByVal PartNumber As String, ByVal SerialNumber As String,
                                    ByVal InstallID As String, ByVal CustomerID As String,
                                    ByVal ContactName As String, ByVal ContactPhone As String,
                                    ByVal CompanyCode As String) As String ',ByVal AlgorithmCode As String

        Dim Emsg As String = ""
        Dim Alg As String = ""

        'If (AlgorithmCode = "") Then
        'Alg = ValidatePartSerial(PartNumber, SerialNumber, Emsg)
        'Else
        'Alg = AlgorithmCode
        'End If

        Alg = ValidatePartSerial(PartNumber, SerialNumber, Emsg)

        If Emsg = "" And Alg <> "" Then

            Dim AuthCode As String = GetAuthCode(InstallID, SerialNumber, Alg, Emsg)

            If AuthCode <> "" And Emsg = "" Then
                GetAthorization = AuthCode
            Else
                Return Emsg
            End If
        Else
            Return Emsg
        End If




    End Function
    Private Function GetAuthCode(ByVal InstallID As String, ByVal SerialNo As String, ByVal Alg As String, ErrorMessage As String) As String
        GetAuthCode = ""
        HttpContext.Current.Application.Lock()
        Try

            If File.Exists(ConfigurationManager.AppSettings("AuthWorkingDirectory") & "\AuthCODE.txt") Then
                File.Delete(ConfigurationManager.AppSettings("AuthWorkingDirectory") & "\AuthCODE.txt")
            End If

            Dim psinfo As New ProcessStartInfo
            Dim Proc As New Process

            psinfo.UseShellExecute = False
            psinfo.WorkingDirectory = ConfigurationManager.AppSettings("AuthWorkingDirectory")
            psinfo.FileName = ConfigurationManager.AppSettings("AuthExe")
            psinfo.Arguments = InstallID & " " & SerialNo & " " & Alg

            psinfo.RedirectStandardError = True
            psinfo.RedirectStandardOutput = True
            psinfo.RedirectStandardInput = True
            Proc.StartInfo = psinfo
            Proc.Start()
            System.Threading.Thread.Sleep("3000")
            Proc.Kill()
            Dim thefile As StreamReader = File.OpenText(ConfigurationManager.AppSettings("AuthWorkingDirectory") & "\AuthCODE.txt")
            GetAuthCode = thefile.ReadToEnd
            thefile.Close()
            If File.Exists(ConfigurationManager.AppSettings("AuthWorkingDirectory") & "\AuthCODE.txt") Then
                File.Delete(ConfigurationManager.AppSettings("AuthWorkingDirectory") & "\AuthCODE.txt")
            End If
            If GetAuthCode.IndexOf(" ") > 0 Then
                ErrorMessage = "Problem obtaining authorization code. Please contact HME to obtain code. <br />For installations in the US, please call 800.848-4468. All others, please call 858.535.6000. <br />Press 1, 2, 3 to be directed to Technical Support."
                GetAuthCode = ""
            End If
            If GetAuthCode.Length <> 8 Then
                ErrorMessage = "Problem obtaining authorization code. Please contact HME to obtain code. <br />For installations in the US, please call 800.848-4468. All others, please call 858.535.6000. <br />Press 1, 2, 3 to be directed to Technical Support."
                GetAuthCode = ""
            End If
        Catch ex As Exception
            'p_errorMessage = ex.Message
            ErrorMessage = "Problem obtaining authorization code. Please contact HME to obtain code. <br />For installations in the US, please call 800.848-4468. All others, please call 858.535.6000. <br />Press 1, 2, 3 to be directed to Technical Support."
        Finally

        End Try
        HttpContext.Current.Application.UnLock()

    End Function
    Private Function ValidatePartSerial(ByVal PartNumber As String, ByVal SerialNo As String, ByRef ErrorMessage As String) As String
        Dim connectionString As String
        connectionString = "data source=powfsasql01;user id=AXCustomerPortal;pwd=hM3AXCu$t0m3rP0rt4l!Pr0d!;initial catalog=Ax2012_hme_migr;"

        Dim conn As New SqlConnection
        Dim dataSet As New DataSet()
        If conn.State = ConnectionState.Closed Then
            conn.ConnectionString = connectionString

        End If

        Try
            conn.Open()

            Dim sqlquery As String = "select hmealgorithmcode from dbo.FSA_ServiceAggrement_Detail where serialnumber= @serialnumber and itemId= @itemid"

            Dim adapter As New SqlDataAdapter
            Dim parameter As New SqlParameter
            Dim command As SqlCommand = New SqlCommand(sqlquery, conn)
            With command.Parameters
                .Add(New SqlParameter("itemId", PartNumber))
                .Add(New SqlParameter("serialnumber", SerialNo))
            End With
            command.Connection = conn
            Dim SqlDataAdapter As New SqlDataAdapter(command)
            SqlDataAdapter.Fill(dataSet)
            If (dataSet.Tables(0).Rows.Count > 0) Then
                Dim alc = dataSet.Tables(0).Rows(0)("hmealgorithmcode")
                If (alc = "") Then
                    ErrorMessage = "Invalid Serial Number and or Part Number"
                    ValidatePartSerial = ""
                Else
                    ValidatePartSerial = alc
                End If
            Else
                ErrorMessage = "Invalid Serial Number and or Part Number"
                ValidatePartSerial = ""
            End If

        Catch ex As Exception
            ErrorMessage = ex.Message
            ValidatePartSerial = ""
        End Try
    End Function

End Class
